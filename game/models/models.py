# -*- coding: utf-8 -*-
from typing import Tuple, List
from odoo import exceptions

from odoo import models, fields, api
import random


class zone(models.Model):
    _name = 'game.zone'
    _description = 'Zone'

    name = fields.Char(string="Nombre", default="Narnia")
    photo = fields.Image(max_width=500, max_height=500, help="Foto de la zona")
    small_photo = fields.Image(max_width=30, max_height=30, related="photo", help="Foto en xicotet de la zona")
    _sql_constraints = [('zonename_uniq', 'unique(name)', 'Nom no disponible')]
    state = fields.Selection([('1', 'Zone'), ('2', 'Clan'), ('3', 'Player'), ('4', 'City'), ('5', 'Event')],
                             default='1')
    clanList = fields.One2many('game.clan', 'zone', help="Llistat de clans de la zona", ondelete="cascade")


class clan(models.Model):
    _name = 'game.clan'
    _description = 'Clan with players'

    photo = fields.Image(max_width=500, max_height=500, help="Foto del clan")
    small_photo = fields.Image(max_width=30, max_height=30, related="photo", help="Foto en xicotet del clan")
    name = fields.Text(string="Clan Name", readonly=False, required=True, help="Nom del clan")
    state = fields.Selection([('1', 'Zone'), ('2', 'Clan'), ('3', 'Player'), ('4', 'City'), ('5', 'Event')],
                             default='2')
    _sql_constraints = [('clanname_uniq', 'unique(name)', 'Nom no disponible')]
    zone = fields.Many2one('game.zone')
    members = fields.One2many('res.partner', 'clan', help="Llistat de membres del clan")


class player(models.Model):
    _name = 'res.partner'
    _inherit = 'res.partner'
    _description = 'Player'

    # name = fields.Text(string="Name", readonly=False, required=True, help="Nom del jugador")
    # _sql_constraints = [('playername_uniq', 'unique(name)', 'Nom no disponible')]
    is_player = fields.Boolean(default=True)
    is_premium = fields.Boolean(default=False)
    photo = fields.Image(max_width=200, max_height=200, help="Foto del jugador")
    small_photo = fields.Image(max_width=30, max_height=30, related="photo", help="Foto en pequeño del jugador")
    wonBattles = fields.Integer(default=0, string="Won Battles", readonly=True, help="Batalles que has guanyat")
    lostBattles = fields.Integer(default=0, string="Lost Battles", readonly=True, help="Batalles que has perdut")
    level = fields.Integer(default=1, string="Level", readonly=True, help="Nivell del jugador")
    skill = fields.Selection([('0', 'Noob'), ('1', 'Not Bad'), ('2', 'Fuc**** Boss')])
    skillType = fields.Char(compute="_skill_level")
    date = fields.Datetime(string="Fecha Creacion", default=lambda self: fields.Datetime.now(),
                           store=True, readonly=True, help="Fetxa de creacio")
    bool = fields.Boolean(string="Otaku", help="Per les rises")
    state = fields.Selection([('1', 'Zone'), ('2', 'Clan'), ('3', 'Player'), ('4', 'City'), ('5', 'Event')],
                             default='3')
    clan = fields.Many2one('game.clan', ondelete='set null')
    event = fields.One2many('game.event', 'player', ondelete="cascade")
    city = fields.One2many('game.city', 'player', ondelete="cascade")
    cityAttack = fields.Many2many('game.city', compute="get_other_citys")

    def filter_city(self, c, p):
        if c.player != p:
            return True
        else:
            return False

    def get_other_citys(self):
        for p in self:
            enemy_citys = self.env['game.city'].search([]).filtered(lambda c: self.filter_city(c, p))

        p.cityAttack = enemy_citys.ids

    def _skill_level(self):
        for s in self:
            if s.skill == '0':
                s.skillType = '0 - Noob'
            elif s.skill == '1':
                s.skillType = '1 - Not Bad'
            elif s.skill == '2':
                s.skillType = '2 - Fuc**** Boss'


class city(models.Model):
    _name = 'game.city'
    _description = 'City of a player'

    name = fields.Char()
    photo = fields.Image(max_width=500, max_height=500, help="Foto de la ciutat")
    _sql_constraints = [('cityname_uniq', 'unique(name)', 'Nom no disponible')]
    player = fields.Many2one('res.partner')
    position = fields.Integer()
    _sql_constraints = [('cityposition_uniq', 'unique(position)', 'Posició no disponible')]

    gold = fields.Float(default=0, string="Gold", readonly=True)
    food = fields.Float(default=0, string="Food", readonly=True)
    city_gold_mines = fields.Integer(default=1, help="Prod=level*5", string="Gold Mine Level)", readonly=True)
    city_food_farm = fields.Integer(default=1, help="Prod=level*5", string="Food Farm Level", readonly=True)

    barracks_level = fields.Integer(default=1, string="Barracks Level", readonly=True)
    mortair_level = fields.Integer(default=1, string="Mortair Level", readonly=True)
    canyon_level = fields.Integer(default=1, string="Canyon Level", readonly=True)

    soldiers = fields.Integer(default=0, string="Soldiers", readonly=True)

    state = fields.Selection([('1', 'Zone'), ('2', 'Clan'), ('3', 'Player'), ('4', 'City'), ('5', 'Event')],
                             default='4')
    cambioscity = fields.One2many('game.cambioscity', 'city')

    def calculate_production(self):
        if self.player.is_premium:
            new_gold = self.city_gold_mines * 8
            new_food = self.city_food_farm * 8
        else:
            new_gold = self.city_gold_mines * 5
            new_food = self.city_food_farm * 5

        self.write({
            'gold': self.gold + new_gold,
            'food': self.food + new_food
        })

        date = fields.Datetime.now()
        cambioscity = self.env['game.cambioscity'].create(
            {'city': self.id, 'date': date, 'name': self.name})

        cambioscity.write({
            'gold': self.gold,
            'food': self.food,
            'city_gold_mines': self.city_gold_mines,
            'city_food_farm': self.city_food_farm,
            'barracks_level': self.barracks_level,
            'mortair_level': self.mortair_level,
            'canyon_level': self.canyon_level,
            'soldiers': self.soldiers
        })

    @api.model
    def update_resources(self):
        print("Augment de recursos")
        citys = self.env['game.city'].search([])

        for c in citys:
            c.calculate_production()

    def reset_resources_action_server(self):
        citys = self.env['game.city'].search([])

        for c in citys:
            c.write({
                'gold': 0.0,
                'food': 0.0
            })

    def add_soldiers(self):
        if self.food >= 50:
            self.soldiers = self.soldiers + 1
            self.food = self.food - 50
        else:
            raise exceptions.Warning('No tienes suficiente comida(50)')

    def upgrade_barracks(self):
        if self.barracks_level < self.player.level:
            if self.gold >= (50 * self.barracks_level):
                self.barracks_level = self.barracks_level + 1
                self.gold = self.gold - (50 * self.barracks_level)
            else:
                raise exceptions.Warning('No tienes suficiente comida')
        else:
            raise exceptions.Warning('El edificio solo puede estar al nivel del jugador')

    def upgrade_mortair(self):
        if self.mortair_level < self.player.level:
            if self.gold >= (50 * self.mortair_level):
                self.mortair_level = self.mortair_level + 1
                self.gold = self.gold - (50 * self.mortair_level)
            else:
                raise exceptions.Warning('No tienes suficiente oro')
        else:
            raise exceptions.Warning('El edificio solo puede estar al nivel del jugador')

    def upgrade_canyon(self):
        if self.canyon_level < self.player.level:
            if self.gold >= (50 * self.canyon_level):
                self.canyon_level = self.canyon_level + 1
                self.gold = self.gold - (50 * self.canyon_level)
            else:
                raise exceptions.Warning('No tienes suficiente oro')
        else:
            raise exceptions.Warning('El edificio solo puede estar al nivel del jugador')

    def upgrade_city_gold_mines(self):
        if self.city_gold_mines < self.player.level:
            if self.gold >= (50 * self.city_gold_mines):
                self.city_gold_mines = self.city_gold_mines + 1
                self.gold = self.gold - (50 * self.city_gold_mines)
            else:
                raise exceptions.Warning('No tienes suficiente oro')
        else:
            raise exceptions.Warning('El edificio solo puede estar al nivel del jugador')

    def upgrade_city_food_farm(self):
        if self.city_food_farm < self.player.level:
            if self.gold >= (50 * self.city_food_farm):
                self.city_food_farm = self.city_food_farm + 1
                self.gold = self.gold - (50 * self.city_food_farm)
            else:
                raise exceptions.Warning('No tienes suficiente comida')
        else:
            raise exceptions.Warning('El edificio solo puede estar al nivel del jugador')


class battle(models.Model):
    _name = 'game.battle'
    _description = 'Battle'

    def _default_player(self):
        return self.env['res.partner'].browse(self._context.get('active_id'))

    name = fields.Char(required=True)
    player = fields.Many2one('res.partner')
    player_1 = fields.Many2one('res.partner', default=_default_player, domain="[('is_player', '=', True)]",
                               ondelete='restrict')
    player_2 = fields.Many2one('res.partner', ondelete='restrict', domain="[('is_player', '=', True)]")
    city_origin = fields.Many2one('game.city', ondelete="restrict")
    city_destination = fields.Many2one('game.city', ondelete="restrict")
    winner = fields.Many2one('res.partner', readonly=True)
    finished = fields.Boolean(default=False)
    state = fields.Selection([('global', 'Global'),
                              ('players', 'Players')],
                             default='global')

    player_1_img = fields.Image(related='player_1.photo')
    player_2_img = fields.Image(related='player_2.photo')
    city_origin_img = fields.Image(related='city_origin.photo')
    city_destination_img = fields.Image(related='city_destination.photo')

    def _create_battle(self):
        new_battle = self.env['game.battle'].create({
            'name': self.name,
            'player_1': self.player_1.id,
            'player_2': self.player_2.id,
            'city_origin': self.city_origin.id,
            'city_destination': self.city_destination.id})
        return {
            'name': 'New battle',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'game.battle',
            'res_id': new_battle.id,
            'context': self._context,
            'type': 'ir.actions.act_window',
            'target': 'current',
        }

    @api.onchange('player_1')
    def _onchange_player1(self):
        if self.player_2:
            if self.player_1.id == self.player_2.id:
                self.player_1 = False
                return {
                    'warning': {
                        'title': "Els jugadors tenen que ser diferents",
                        'message': "Has elegit el mateix jugador"
                    }
                }
        return {
            'domain': {'city_origin': [('player', '=', self.player_1.id)],
                       'player_2': [('id', '!=', self.player_1.id)]}
        }

    @api.onchange('player_2')
    def _onchange_player2(self):
        if self.player_1:
            if self.player_1.id == self.player_2.id:
                self.player_2 = False
                return {
                    'warning': {
                        'title': "Els jugadors tenen que ser diferents",
                        'message': "Has elegit el mateix jugador",
                    }
                }
        return {
            'domain': {'city_destination': [('player', '=', self.player_2.id)],
                       'player_1': [('id', '!=', self.player_2.id)]},
        }

    def battle(self):
        battles = self.search([('finished', '=', False)])
        for s in battles:

            if s.city_origin.soldiers != 0 and s.city_destination.soldiers != 0:

                if s.city_origin.soldiers > s.city_destination.soldiers:
                    s.city_origin.soldiers = int(s.city_origin.soldiers - s.city_destination.soldiers)
                    s.city_destination.soldiers = 0

                    s.player_1.wonBattles = int(s.player_1.wonBattles + 1)
                    s.player_2.lostBattles = int(s.player_2.lostBattles + 1)
                    self.winner = s.player_1

                if s.city_destination.soldiers > s.city_origin.soldiers:
                    s.city_destination.soldiers = int(s.city_destination.soldiers - s.city_origin.soldiers)
                    s.city_origin.soldiers = 0

                    s.player_2.wonBattles = int(s.player_2.wonBattles + 1)
                    s.player_1.lostBattles = int(s.player_1.lostBattles + 1)
                    self.winner = s.player_2

                if s.city_origin.soldiers == s.city_destination.soldiers:
                    a = random.randint(1, 2)

                    if a == 1:
                        s.city_origin.soldiers = random.randint(1, int(s.city_origin.soldiers))
                        s.city_destination.soldiers = 0

                        s.player_1.wonBattles = int(s.player_1.wonBattles + 1)
                        s.player_2.lostBattles = int(s.player_2.lostBattles + 1)
                        self.winner = s.player_1
                    else:
                        s.city_destination.soldiers = random.randint(1, int(s.city_destination.soldiers))
                        s.city_origin.soldiers = 0

                        s.player_2.wonBattles = int(s.player_2.wonBattles + 1)
                        s.player_1.lostBattles = int(s.player_1.lostBattles + 1)
                        self.winner = s.player_2

                self.finished = True
            else:
                return {
                    'warning': {
                        'title': "Error en disputar batalla",
                        'message': "No se puede disputar una batalla si un jugador no tiene soldados"
                    }
                }


class battle_wizard(models.TransientModel):
    _name = 'game.battle_wizard'

    def _default_player(self):
        return self.env['res.partner'].browse(self._context.get('active_id'))

    name = fields.Char(required=True)
    player_1 = fields.Many2one('res.partner', default=_default_player, domain="[('is_player', '=', True)]",
                               ondelete='restrict', readonly=True)
    player_2 = fields.Many2one('res.partner', ondelete='restrict', domain="[('is_player', '=', True)]")
    city_origin = fields.Many2one('game.city', ondelete="restrict", domain="[('player', '=', player_1)]")
    city_destination = fields.Many2one('game.city', ondelete="restrict")
    winner = fields.Many2one('res.partner')
    finished = fields.Boolean(default=False)
    state = fields.Selection([('global', 'Global'),
                              ('players', 'Players')],
                             default='global')

    player_1_img = fields.Image(related='player_1.photo')
    player_2_img = fields.Image(related='player_2.photo')
    city_origin_img = fields.Image(related='city_origin.photo')
    city_destination_img = fields.Image(related='city_destination.photo')

    _sql_constraints: List[Tuple[str, str, str]] = [
        ('name_uniq', 'unique(name)', 'El nombre ya existe, prueba con otro'), ]

    def create_battle(self):
        new_battle = self.env['game.battle'].create({
            'name': self.name,
            'player_1': self.player_1.id,
            'player_2': self.player_2.id,
            'city_origin': self.city_origin.id,
            'city_destination': self.city_destination.id
        })
        return {
            'name': 'New battle',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'game.battle',
            'res_id': new_battle.id,
            'context': self._context,
            'type': 'ir.actions.act_window',
            'target': 'current',
        }

    @api.onchange('player_2')
    def _onchange_player2(self):
        if self.player_1:
            if self.player_1.id == self.player_2.id:
                self.player_2 = False
                return {
                    'warning': {
                        'title': "Els jugadors tenen que ser diferents",
                        'message': "Has elegit el mateix jugador",
                    }
                }
        return {
            'domain': {'city_destination': [('player', '=', self.player_2.id)],
                       'player_1': [('id', '!=', self.player_2.id)]},
        }

    def previous(self):
        if self.state == 'players':
            self.state = 'global'
        return {
            'name': "Battle Wizard",
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'game.battle_wizard',
            'res_id': self.id,
            'context': self._context,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }

    def next(self):
        if self.state == 'global':
            self.state = 'players'
        return {
            'name': "Battle Wizard",
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'game.battle_wizard',
            'res_id': self.id,
            'context': self._context,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }


class event(models.Model):
    _name = 'game.event'
    _description = 'Event for use View Calendar'

    player = fields.Many2one('res.partner', ondelete='cascade')
    name = fields.Char(string="Evento: ")
    _sql_constraints = [('eventname_uniq', 'unique(name)', 'Nom no disponible')]
    start = fields.Datetime()
    finish = fields.Datetime()
    duration = fields.Float(string="Duration(Hours)", compute="cambiar_duracion", readonly=True)
    state = fields.Selection([('1', 'Zone'), ('2', 'Clan'), ('3', 'Player'), ('4', 'City'), ('5', 'Event')],
                             default='5')

    @api.onchange('finish')
    def _onchange_finish(self):
        return {
            'warning': {
                'title': "Cambio detectado",
                'message': "Se ha cambiado la fecha de finalizacion",
            },
        }

    def cambiar_duracion(self):
        for p in self:
            diferencia = p.finish - p.start
            diferencia_horas = diferencia.total_seconds() / 3600
            p.duration = diferencia_horas


class event_wizard(models.TransientModel):
    _name = 'game.event_wizard'

    player = fields.Many2one('res.partner', ondelete='cascade')
    name = fields.Char(string="Evento: ")
    _sql_constraints = [('eventname_uniq', 'unique(name)', 'Nom no disponible')]
    start = fields.Datetime()
    finish = fields.Datetime()
    duration = fields.Float(string="Duration(Hours)", compute="cambiar_duracion", readonly=True)
    state = fields.Selection([('1', 'Zone'), ('2', 'Clan'), ('3', 'Player'), ('4', 'City'), ('5', 'Event')],
                             default='5')

    def cambiar_duracion(self):
        for p in self:
            diferencia = p.finish - p.start
            diferencia_horas = diferencia.total_seconds() / 3600
            p.duration = diferencia_horas

    def create_event(self):
        new_event = self.env['game.event'].create({
            'name': self.name,
            'start': self.start,
            'finish': self.finish,
        })
        return {
            'name': 'New event',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'game.event',
            'res_id': new_event.id,
            'context': self._context,
            'type': 'ir.actions.act_window',
            'target': 'current',
        }


# Falta eliminar event una vegada haja finalitzat

class error_wizard(models.TransientModel):
    _name = 'game.error_wizard'

    error = fields.Char(string="Error", readonly=True)


class cambioscity(models.Model):
    _name = 'game.cambioscity'
    _description = 'Cambios en las ciudades'

    name = fields.Char()
    city = fields.Many2one('game.city', ondelete="cascade", required=True)

    date = fields.Char()
    gold = fields.Float()
    food = fields.Float()
    city_gold_mines = fields.Integer()
    city_food_farm = fields.Integer()
    barracks_level = fields.Integer()
    mortair_level = fields.Integer()
    canyon_level = fields.Integer()
    soldiers = fields.Integer()
